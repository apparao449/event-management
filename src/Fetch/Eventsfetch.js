import React, { Component } from 'react'
import axios from 'axios'
import './Eventfetch.css'
export default class EventFetch extends Component {
    constructor(){
        super();
        this.state={
            Events:[]
        }
    }
    componentDidMount(){
        axios.get('http://localhost:3007/fetch').then((regdata)=>{
            this.setState({Events:regdata.data})
        })
    }
  render() {
    return (
      <div id='ef'>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="#">Event Management System</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                    <li style={{marginLeft:"300px"}} class="nav-item">
                            <a class="nav-link" href="/admineventselection">Home</a>
                        </li>
                        <li style={{marginLeft:"300px"}} class="nav-item">
                            <a class="nav-link" href="/fetch">User Data</a>
                        </li>
                        <li style={{marginLeft:"330px"}} class="nav-item">
                            <a class="nav-link" href="/">logout</a>
                        </li>
                    </ul>
                </div>
            </nav>
        <div>
        <table className='table table-bordered'>
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Emailid</th>
                    <th>Phone Number</th>
                    <th>Password</th>
                </tr>
            </thead>
            <tbody>
                {
                    this.state.Events.map((reg)=>(
                        <tr>
                            <td>{reg.Event}</td>
                            <td>{reg.lastname}</td>
                            <td>{reg.email}</td>
                            <td>{reg.phonenumber}</td>
                            <td>{reg.password}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
      </div>
      </div>
    ) 
  }
}