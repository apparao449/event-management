import React, { Component } from 'react'
import axios from 'axios'
export default class Fetch extends Component {
    constructor(){
        super();
        this.state={
            records:[]
        }
    }
    componentDidMount(){
        axios.get('http://localhost:3007/fetch').then((regdata)=>{
            this.setState({records:regdata.data})
        })
    }
  render() {
    return (
      <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="#"><em>Event Management</em></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                    <li style={{marginLeft:"300px"}} class="nav-item">
                            <a class="nav-link" href="/admineventselection">Home</a>
                        </li>
                        <li style={{marginLeft:"330px"}} class="nav-item">
                            <a class="nav-link" href="/eventfetch">Events Registered</a>
                        </li>
                        <li style={{marginLeft:"330px"}} class="nav-item">
                            <a class="nav-link" href="/">logout</a>
                        </li>
                    </ul>
                    {/* <input style={{ marginLeft: "550px" }} class="form-control mr-sm-2" type="search" placeholder="Search" /> */}
                    {/* <button style={{ marginLeft: "20px", marginRight: "10px" }} class="btn btn-success" type="submit">Search</button> */}
                </div>
            </nav>
        <div>
        <table className='table table-bordered'>
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Emailid</th>
                    <th>Phone Number</th>
                    <th>Password</th>
                </tr>
            </thead>
            <tbody>
                {
                    this.state.records.map((reg)=>(
                        <tr>
                            <td>{reg.firstname}</td>
                            <td>{reg.lastname}</td>
                            <td>{reg.email}</td>
                            <td>{reg.phonenumber}</td>
                            <td>{reg.password}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
      </div>
      </div>
    ) 
  }
}