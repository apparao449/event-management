import React, { useState } from 'react';
import Chip from '@mui/material/Chip';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';

const WeddingThemeSelection = () => {
  const [chipArray, setChipArray] = useState([
    { name: 'Haldi', selected: false, image: 'https://www.theknot.com/tk-media/images/b0e84a56-db35-4752-acdd-c8d39c2743cc~rs_768.h', details: 'Haldi details' },
    { name: 'Mehendi', selected: false, image:'https://mahastudiosinc.com/wp-content/uploads/sites/28335/2022/11/What-is-a-Mehndi-Party-Guide.jpg', details: 'Mehendi details' },
    { name: 'Sangeet', selected: false, image: 'https://shaadiwish.com/blog/wp-content/uploads/2019/09/Untitled-design-12.png', details: 'Sangeet details' },
    { name: 'Marriage', selected: false, image: 'https://www.voj.news/wp-content/uploads/2016/05/Marriages-in-different-religions.jpg', details: 'Marriage details' },
    { name: 'Reception', selected: false, image: 'https://www.rentmywedding.com/Images/Podcast/Best-Wedding-Decor.jpg', details: 'Reception details' },
    { name: 'Makeup', selected: false, image: 'https://www.weddingplz.com/blog/wp-content/uploads/airbrush-makeup-weddingplz-feature-685x400.jpg', details: 'Makeup details' },
  ]);

  const [selectedOptions, setSelectedOptions] = useState([]);

  const handleChipClick = (index) => {
    const updatedChips = [...chipArray];
    updatedChips[index].selected = !updatedChips[index].selected;

    if (updatedChips[index].selected) {
      setSelectedOptions([...selectedOptions, updatedChips[index]]);
    } else {
      const filteredOptions = selectedOptions.filter((option) => option.name !== updatedChips[index].name);
      setSelectedOptions(filteredOptions);
    }

    setChipArray(updatedChips);
  };

  const handleButtonClick = () => {
    console.log('Selected Ceremonies:', selectedOptions);
    alert("selected Successfully")
    //we can add any additional logic you want to perform on button click
  };

  return (
    <div style={{backgroundColor:'transparent'}}>
      <Stack direction="row" spacing={4} paddingTop={2} paddingLeft={8}>
        {chipArray.map((item, index) => (
          <Chip
            key={index}
            label={item.name}
            variant="outlined"
            onClick={() => handleChipClick(index)}
            size="large"
            className="template_chip"
            style={{
              backgroundColor: item.selected ? '#1976d2' : '#fff',
              color: item.selected ? '#fff' : '#1976d2',
              height:'54px',
              width:'150px',
              borderRadius:'20px',
              fontFamily:'timesnewroman'
            }}
            avatar={<Avatar alt={item.name} src={item.image} style={{height:'40px',width:'40px'}} />}
          />
        ))}
      </Stack>

      <div style={{ marginTop: '20px', borderRadius:'15px'}}>
     
        <ul style={{ paddingTop: '10px', display: 'flex', listStyleType: 'none', flexWrap: 'wrap' }}>
          {selectedOptions.map((option) => (
            <li key={option.name} style={{ marginRight: '15px', marginBottom: '15px', textAlign: 'center',fontFamily:'timesnewroman'}}>
              <img
                src={option.image}
                alt={option.name}
                style={{ marginRight: '20px', height: '175px', width: '254px', borderRadius: '15px', border: '1px solid black' }}
              />
              <p style={{ fontSize: '20px' }}>{option.name}</p>
              <p>{option.details}</p>
            </li>
          ))}
        </ul>
        <div style={{ textAlign: 'center', marginTop: '20px',paddingBottom:'30px' }}>
          <Button variant="contained" color="primary" onClick={handleButtonClick}>Submit</Button>
        </div>
      </div>
      <br/>
      <br/>
      <br/>
    </div>
  );
};

export default WeddingThemeSelection;