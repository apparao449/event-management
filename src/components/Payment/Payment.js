import React from 'react'
import Typography from '@mui/material/Typography';
import { useState } from 'react';
import { TextField } from '@mui/material';
import Button from '@mui/material/Button';

const Payment = () => {

    const [Payment, setPayment] = useState("")

    const handleAmount = (e) => {
        const value = e.target.value;
        setPayment(value)
    }
    const handlePayment = () => {

        Payment ===  "" ? alert("Enter amount ") :
        alert(`Your event has succesfully submitted, we will get in touch with you`)
        console.log(`${Payment} paid`)
    }

    // const handleSubmit = () => {
    //     console.log(`${Payment} paid`)
    // }
    return (
        <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>

            <TextField id="outlined-basic" label="Initial Deposit : 5000" variant="outlined"
                value={Payment}
                onChange={(e) => handleAmount(e)} />

            <Button variant="outlined" style={{ marginTop: "2rem" }} onClick={handlePayment}>Pay</Button>


            {/* <Button  variant="outlined" style={{ marginTop: "5rem" }}onClick={handleSubmit}>
                Submit
            </Button> */}
            <br/>
            <br/>
            <br/>
            <br/>
        </div>
    )
}

export default Payment