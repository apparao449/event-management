import React, { useState } from 'react'
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';

function BirthdayThemeSelection() {
  const [image, setImage] = useState("")
  const handleImage = (e) =>{
    const value = e.target.alt
    setImage(value);
    console.log(image)
  }
  const itemData = [
    {
      img: 'https://i.pinimg.com/564x/3d/1e/73/3d1e73a92e5b3ac7dbc04c57a8785029.jpg',
      title: 'Ballon Ring',
      author: 'Starting at 5000$',
    },
    {
      img: 'https://i.pinimg.com/564x/20/52/95/2052958c5fa2f2f36458cb53af99f7e1.jpg',
      title: 'Ballon Arch',
      author: 'Starting at 6000$',
    },
    {
      img: 'https://i.pinimg.com/564x/26/b2/09/26b209de4bfa0f9f26ae43ecf4da9707.jpg',
      title: 'Classic',
      author: 'Starting at 4000$',
    },
    {
      img: 'https://i.pinimg.com/564x/3b/20/a7/3b20a7a1ffb2e2993f07df4c784fd7ee.jpg',
      title: 'Ballon Stand',
      author: 'Starting at 7000$',
    },


  ];
  return (
    <div style={{display: "grid", alignItems: "center", gridTemplateColumns: "1fr 1fr 1fr", columnGap: "100px" }}>
      <div>
        <span> Birthady Theme</span>

      </div>
    

    <div style={{marginLeft:"500px",marginTop:"2rem"}}>
         <ImageList sx={{ width: 700, height: 500 }}>
      {itemData.map((item, index) => (
        <ImageListItem key={item.img}>
          <img
            srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
            src={`${item.img}?w=248&fit=crop&auto=format`}
            alt={item.title}
            loading="lazy"
            onClick={handleImage}
          />
          <ImageListItemBar
            title={item.title}
            subtitle={<span>cost: {item.author}</span>}
            position="below"
          />
        </ImageListItem>
      ))}
    </ImageList>
  </div>
  </div>
  )
}


export default  BirthdayThemeSelection