import React, { useState } from 'react';
import axios from 'axios';
import './Registration.css';
import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap/dist/css/bootstrap.min.css'
export default function Registration() {
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [phonenumber, setPhonenumber] = useState('');
    const [password, setPassword] = useState('');

    const [firstnameError, setFirstnameError] = useState('');
    const [lastnameError, setLastnameError] = useState('');
    const [emailError, setEmailError] = useState('');
    const [phonenumberError, setPhonenumberError] = useState('');
    const [passwordError, setPasswordError] = useState('');

    const validateForm = () => {
        let isValid = true;

        if (!firstname) {
            setFirstnameError('*First Name is required');
            isValid = false;
        } else {
            setFirstnameError('');
        }

        if (!lastname) {
            setLastnameError('*Last Name is required');
            isValid = false;
        } else {
            setLastnameError('');
        }

        if (!email || !email.includes('@gmail.com')) {
            setEmailError('*Enter a valid Gmail address');
            isValid = false;
        } else {
            setEmailError('');
        }

        if (!phonenumber || !/^\d{10}$/.test(phonenumber)) {
            setPhonenumberError('*Phone Number must be 10 digits');
            isValid = false;
        } else {
            setPhonenumberError('');
        }

        if (
            !password ||
            password.length < 8 ||
            !/[A-Z]/.test(password) || // At least one uppercase letter
            !/\d/.test(password) || // At least one digit
            !/[!@#$%^&*(),.?":{}|<>]/.test(password) // At least one special character
        ) {
            setPasswordError(
                '*Password must be at least 8 characters and contain one uppercase letter, one digit, and one special character'
            );
            isValid = false;
        } else {
            setPasswordError('');
        }

        return isValid;
    };

    const HandleRegister = async () => {
        if (validateForm()) {
            try {
                const response = await axios.post('http://localhost:3007/register', {
                    firstname,
                    lastname,
                    email,
                    phonenumber,
                    password,
                });

            //     if (response.data) {
            //         alert('Registration Successful');
            //     }
            // } catch (error) {
            //     alert('Registration Failed', error.message);
            // }
            if (response.data.success) {
                alert('Registration Successful');
            } else {
                alert('Registration Failed: ' + response.data.message);
            }
        } catch (error) {
            alert('Registration Failed: Email Already Exists, Use Another Email.');
        }
        }
    };

    return (
        <div id='reg'>
            <nav id='nav1' class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="#"><em>Event Management</em></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                                
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        <div  className='container box' style={{marginLeft : "35rem"}}>
            <h1>User Registration</h1>
            <div className='form-group'>
                <label>First Name:</label>
                <input
                    type='text'
                    className='form-control'
                    value={firstname}
                    onChange={(e) => {
                        setFirstname(e.target.value);
                    }}
                />
                <div style={{color:"red"}} className='error-message'>{firstnameError}</div>
            </div>
            <div className='form-group'>
                <label>Last Name:</label>
                <input
                    type='text'
                    className='form-control'
                    value={lastname}
                    onChange={(e) => {
                        setLastname(e.target.value);
                    }}
                />
                <div style={{color:"red"}} className='error-message'>{lastnameError}</div>
            </div>
            <div className='form-group'>
                <label>Email:</label>
                <input
                    type='text'
                    className='form-control'
                    value={email}
                    onChange={(e) => {
                        setEmail(e.target.value);
                    }}
                />
                <div style={{color:"red"}} className='error-message'>{emailError}</div>
            </div>
            <div className='form-group'>
                <label>Phone Number:</label>
                <input
                    type='text'
                    className='form-control'
                    value={phonenumber}
                    onChange={(e) => {
                        setPhonenumber(e.target.value);
                    }}
                />
                <div style={{color:"red"}} className='error-message'>{phonenumberError}</div>
            </div>
            <div className='form-group'>
                <label>Password:</label>
                <input
                    type='password'  // Change to password type for sensitive information
                    className='form-control'
                    value={password}
                    onChange={(e) => {
                        setPassword(e.target.value);
                    }}
                />
                <div style={{color:"red"}} className='error-message'>{passwordError}</div>
            </div>
            <button className='btn btn-primary' onClick={HandleRegister}>
                Register
            </button>
            <br/>
            <br/>
            <label style={{marginLeft:"100px"}}>Already have an Account..?</label>
            <a style={{marginLeft:"10px"}} href='/userlogin'>Login</a>
        </div>
        <br/>
        
        </div>
    );
}