import React, { useState } from 'react'
import axios from 'axios'
import './Login.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap/dist/css/bootstrap.min.css'
import { $CombinedState } from 'redux'
export default function Login2() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const HandleLogin = async () => {
        
            const res = await axios.get(`http://localhost:3007/userlogin/${email}/${password}`)
            const resp = await axios.get(`http://localhost:3007/adminlogin/${email}/${password}`)

            if(res.data||resp.data){

                if(res.data){
                    {window.location = "http://localhost:3000/eventselection";}
                }
                if(resp.data){
                    {window.location = "http://localhost:3000/admineventselection";}
                }
            }
            else{
                alert("Invalid Credentials")
            }
    }
    return (
        <div id='login'>
            <nav id='nav1' class="navbar navbar-expand-lg ">
                <a class="navbar-brand" href="#"><em>Event Management</em></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                                
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        <div id='forml' className='container box' style={{marginLeft : "35rem"}}>
            <h1>Login</h1>

            <div className='form-group'>
                <label>Email:</label>
                <input
                    type='text'
                    className='form-control'
                    value={email}
                    onChange={(e) => {
                        setEmail(e.target.value);
                    }}
                />
            </div>
            <div className='form-group'>
                <label>Password:</label>
                <input
                    type='password' 
                    className='form-control'
                    value={password}
                    onChange={(e) => {
                        setPassword(e.target.value);
                    }}
                />
            </div>
            <button className='btn btn-primary' onClick={HandleLogin}>
                Login
            </button>
            <br/>
            <br/>
            <label style={{marginLeft:"150px"}}>New User..?</label>
            <a style={{marginLeft:"10px"}} href='/register'>Sign Up</a>
        </div>
        <br/>
        <br/>
        <br/>
        </div>
    )
}