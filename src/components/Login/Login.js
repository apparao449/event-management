import React, { useState } from 'react'
import axios from 'axios'
import './Login.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap/dist/css/bootstrap.min.css'
export default function Login() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const HandleLogin = async () => {
            const res = await axios.get(`http://localhost:3007/adminlogin/${email}/${password}`)
            if(res.data){
                {window.location = "http://localhost:3000/admineventselection";}
            }
            else{
                alert("Invalid Credentials")
            }
        

    }
    return (
        <div>
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="#">Event Management System</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Home
                                
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contactus">ContactUs</a>
                        </li>
                    </ul>
                </div>
            </nav>
        <div className='container box' style={{marginLeft : "35rem"}}>
            <h1>Login Form</h1>

            <div className='form-group'>
                <label>Email:</label>
                <input
                    type='text'
                    className='form-control'
                    value={email}
                    onChange={(e) => {
                        setEmail(e.target.value);
                    }}
                />
            </div>
            <div className='form-group'>
                <label>Password:</label>
                <input
                    type='password' 
                    className='form-control'
                    value={password}
                    onChange={(e) => {
                        setPassword(e.target.value);
                    }}
                />
            </div>
            <button className='btn btn-primary' onClick={HandleLogin}>
                Login
            </button>
        </div>
        </div>
    )
}