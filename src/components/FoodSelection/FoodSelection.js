import React from 'react'
import { useState } from 'react';
import Chip from '@mui/material/Chip';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import Avatar from "@mui/material/Avatar"
import Button from "@mui/material/Button"


const FoodSelection = () => {
    const [chipArray,setChipArray]=useState([
        {name:'Veg', selected:'false',image:'https://www.eastcoastdaily.in/wp-content/uploads/2017/08/sanjeevanam-chen.jpg',details:'Dum Aloo, Vegetable Jalfrezi, Paneer Butter Masala, Kadhai paneer, Paneer Lababdaar, Punjabi Kadhi Pakora, Mix Vegetable, Kurmuri Bhindi, Gobhi Keema Matar, Palak Corn, Special Sarson Ka Saag (seasonal), Pindi Channa Masala, Malai Kofta Curry, Gobhi Laccha Adraki, Dal Makhani, Shahi Rajma Masala...etc'},
        {name:'Non-veg',selected:'false',image:'https://www.shutterstock.com/shutterstock/photos/2323268643/display_1500/stock-photo-kerala-special-food-non-veg-sadhya-in-banana-leaf-plate-with-chicken-fry-duck-roast-beef-fry-fish-2323268643.jpg',details:'Kadhai Chicken, Butter Chicken, Chicken Korma, Chicken do Pyaza, Chilli Chicken Gravy, Mutton Rara, Mutton Korma, Mutton Cury, Mutton Roganjosh, Fish Curry, Fish fry...etc'},
        {name:'Combo',selected:'false',image:'https://gogalora.com/wp-content/uploads/listing-uploads/gallery/2020/08/DE9EB75A-511D-476A-AD6B-D2CD533E5493.jpeg',details:'Here in the combination option all items which are in the both veg and Non-veg will be served according to customer choices, extra food items will be add if customer wants'},
    ]);

    const [selectedOptions,setSelectedOptions]=useState([]);

    const handleChipClick = (index) => {
        const updatedChips = [...chipArray];
        updatedChips[index].selected = !updatedChips[index].selected;
    
    
    if (updatedChips[index].selected) {
        setSelectedOptions([...selectedOptions, updatedChips[index]]);
      } else {
        const filteredOptions = selectedOptions.filter((option) => option.name !== updatedChips[index].name);
        setSelectedOptions(filteredOptions);
      }
  
      setChipArray(updatedChips);
    };
  
    const handleButtonClick = () => {
      console.log('Selected photography', selectedOptions);
      alert("selected succesfully")
      //we can add any additional logic you want to perform on button click
    };
  
  return (
    <div style={{backgroundColor:'transparent'}}>
        <h2 style={{fontFamily:'timesnewroman',paddingLeft:'40px',paddingTop:'20px'}}>Food Types</h2>
        <Stack direction="row" spacing={1} paddingLeft={5} paddingTop={1} >
          {chipArray.map((item, index) => (
            <Chip
              key={index}
              label={item.name}
              variant="outlined"
              onClick={() => handleChipClick(index)}
              size="large"
              className="template_chip"
              style={{
                backgroundColor: item.selected ? '#1976d2' : '#fff',
                color: item.selected ? '#fff' : '#1976d2',
                height:'54px',
                width:'200px',
                borderRadius:'20px',
                fontFamily:'timesnewroman'
              }}
              avatar={<Avatar alt={item.name} src={item.image} style={{height:'40px',width:'40px'}} />}
            />
          ))}
        </Stack>
  
        <div style={{ marginTop: '20px', borderRadius:'15px'}}>
       
          <ul style={{ paddingTop: '10px', display: 'flex', listStyleType: 'none', flexWrap: 'wrap' }}>
            {selectedOptions.map((option) => (
              <li key={option.name} style={{ marginRight: '15px', marginBottom: '15px', textAlign: 'center',fontFamily:'timesnewroman'}}>
                <img
                  src={option.image}
                  alt={option.name}
                  style={{ marginRight: '20px', height: '175px', width: '254px', borderRadius: '15px', border: '1px solid black' }}
                />
                <p style={{ fontSize: '20px' }}>{option.name}</p>
                <p>{option.details}</p>
              </li>
            ))}
          </ul>
          <div style={{ textAlign: 'center', marginTop: '20px',paddingBottom:'30px' }}>
            <Button variant="contained" color="primary" onClick={handleButtonClick}>Submit</Button>
          </div>
        </div>
        <br/>
        <br/>
        <br/>
      </div>
  )
}



export default FoodSelection