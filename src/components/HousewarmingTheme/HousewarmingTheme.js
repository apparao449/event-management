import React, { useState } from 'react'
import './HousewarmingTheme.css'
export const HousewarmingTheme = () => {

     const [ image, setImage] = useState("")
     const Itemlist = [
        {
            src: "https://i.pinimg.com/564x/6a/d0/70/6ad0704f0b6a559f0b161e73fd621800.jpg",
            themeType: "Traditional Theme"
        },
        {
            src: "https://i.pinimg.com/564x/9c/24/23/9c2423978fe584b4ddaba22ed0348535.jpg",
            themeType: "Classic Theme"
        },

        {
            src: "https://i.pinimg.com/564x/35/7e/ac/357eacec64024b0d552329e185fa6fc1.jpg",
            themeType: "Vintage Theme"
        },
        {
            src:
                "https://i.pinimg.com/564x/0d/9d/19/0d9d192af21c334b55b64708f77c22c1.jpg",
            themeType: "Modern Theme"
        }
    ]
    const handleImage = (e) =>{
        const value = e.target.src;
        setImage(value);
        console.log(image)

    }

    return (
        <div className='main-div'>
            {Itemlist.map((item, index) => (
                <div class="card" style={{ width: "300px" }} onClick={handleImage}>
                    <img class="card-img-top" src={item.src} alt="Card image cap" />
                    <div class="card-body">
                        <h5 class="card-title">{item.themeType}</h5>
                        
                    </div>
                  




                </div>
            ))}
            {/* <div class="card" style={{ width: "300px" }}>
                <img class="card-img-top" src="https://i.pinimg.com/564x/9c/24/23/9c2423978fe584b4ddaba22ed0348535.jpg" alt="Card image cap" />
                <div class="card-body">
                    <h5 class="card-title">Classic Theme</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              
              
            </div>
            <div class="card" style={{ width: "300px" }}>
                <img class="card-img-top" src="https://i.pinimg.com/564x/35/7e/ac/357eacec64024b0d552329e185fa6fc1.jpg" alt="Card image cap" />
                <div class="card-body">
                    <h5 class="card-title">Vintage Theme</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
               
              
            </div>
            <div class="card" style={{ width: "300px" }}>
                <img class="card-img-top" src="https://i.pinimg.com/564x/0d/9d/19/0d9d192af21c334b55b64708f77c22c1.jpg" alt="Card image cap" />
                <div class="card-body">
                    <h5 class="card-title">Modern Theme</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              
            </div>  */}
        </div>
    )
}

