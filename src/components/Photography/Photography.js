import React from 'react';
import Chip from '@mui/material/Chip';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import { useState } from 'react';

const Photography = () => {
  const [chipArray, setChipArray] = useState([
    { name: 'Photography', selected: false, image: 'https://modernweddings.com/wp-content/uploads/2020/08/AdobeStock_283827644-980x653.jpeg', details: 'Photography details' },
    { name: 'Videography', selected: false, image:'https://static.wixstatic.com/media/efdf22_a732854cd02a499e9e91d7a46bbec830~mv2.jpg/v1/fill/w_1600,h_1068,al_c/efdf22_a732854cd02a499e9e91d7a46bbec830~mv2.jpg', details: 'Videography details' },
    { name: 'Candid Photography', selected: false, image: 'http://1.bp.blogspot.com/-nLhoxrYR6L4/T_L7Y7s-L_I/AAAAAAAADlg/aFSQ2xxwoT4/s640/candid+wedding+photography+chennai+01.jpg', details: 'Candid Photography details' },
    { name: 'Candid Videography', selected: false, image: 'https://cdn0.weddingwire.in/article/4985/original/1280/jpg/25894-multicultural-wedding-dreamcatchers-photography-engage-both-cultures.webp', details: 'Candid Videography details' }
  ]);

  const [selectedOptions, setSelectedOptions] = useState([]);

  const handleChipClick = (index) => {
    const updatedChips = [...chipArray];
    updatedChips[index].selected = !updatedChips[index].selected;

    if (updatedChips[index].selected) {
      setSelectedOptions([...selectedOptions, updatedChips[index]]);
    } else {
      const filteredOptions = selectedOptions.filter((option) => option.name !== updatedChips[index].name);
      setSelectedOptions(filteredOptions);
    }

    setChipArray(updatedChips);
  };

  const handleButtonClick = () => {
    console.log('Selected photography', selectedOptions);
    alert("Selected Successfully")
    //we can add any additional logic you want to perform on button click
  };
  return (
    <div style={{backgroundColor:'transparent'}}>
      <h2 style={{fontFamily:'timesnewroman',paddingLeft:'40px',paddingTop:'20px'}}>Photography Styles</h2>
      <Stack direction="row" spacing={1} paddingLeft={5} paddingTop={1}>
        {chipArray.map((item, index) => (
          <Chip
            key={index}
            label={item.name}
            variant="outlined"
            onClick={() => handleChipClick(index)}
            size="large"
            className="template_chip"
            style={{
              backgroundColor: item.selected ? '#1976d2' : '#fff',
              color: item.selected ? '#fff' : '#1976d2',
              height:'54px',
              width:'200px',
              borderRadius:'20px',
              fontFamily:'timesnewroman'
            }}
            avatar={<Avatar alt={item.name} src={item.image} style={{height:'40px',width:'40px'}} />}
          />
        ))}
      </Stack>

      <div style={{ marginTop: '20px', borderRadius:'15px'}}>
    {/* {  selectedOptions === null  ?  <Typography variant="h6" style={{marginTop:'20px',fontFamily:'timesnewroman',marginLeft:'40px',fontSize:'30px'}}>Selected Photography Styles</Typography> : "" } */}
        <ul style={{ paddingTop: '10px', display: 'flex', listStyleType: 'none', flexWrap: 'wrap' }}>
          {selectedOptions.map((option) => (
            <li key={option.name} style={{ marginRight: '15px', marginBottom: '15px', textAlign: 'center',fontFamily:'timesnewroman'}}>
              <img
                src={option.image}
                alt={option.name}
                style={{ marginRight: '20px', height: '175px', width: '254px', borderRadius: '15px', border: '1px solid black' }}
              />
              <p style={{ fontSize: '20px' }}>{option.name}</p>
              <p>{option.details}</p>
            </li>
          ))}
        </ul>
        <div style={{ textAlign: 'center', marginTop: '20px',paddingBottom:'30px' }}>
          <Button variant="contained" color="primary" onClick={handleButtonClick}>Submit</Button>
        </div>
      </div>
      <br/>
      <br/>
    </div>
  );
}

export default Photography