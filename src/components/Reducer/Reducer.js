const {createStore} = require('redux');

const initialstate = {
    Event:"",
    ThemeSelection:[],
    Photography:[],
    Food:""
}

const reducer = (state=initialstate,action)=>{

    if(action.type==='addevent'){
        return {...state, Event:state.Event += action.payload}
    }
    if(action.type==='addtheme'){
        return {...state, ThemeSelection:[...state.ThemeSelection,{text:action.payload}]}
    }
    
    if(action.type==='addphotography'){
        return {...state, Photography:[...state.Photography,{text:action.payload}]}
    }
    if(action.type==='addfood'){
        return {...state, Food:state.Food += action.payload}
    }
};


const store = createStore(reducer);
export default store;