import React from 'react'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap/dist/css/bootstrap.min.css'
import './HomePage.css'
import { colors } from '@mui/material'
export const HomePage = () => {
    return (
        <div id='main'>
            <div>
                <nav id='nav2' class="navbar navbar-expand-lg ">
                    <a class="navbar-brand" href="#"><em>Event Management</em></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarColor01">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="/">Home
                                </a>
                            </li>
                            <li style={{marginLeft:"800px"}} class="nav-item">
                                <a class="nav-link" href="/userlogin">login
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div className='carousel-division'>
                <div id="carouselExampleCaptions" class="carousel slide">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="https://images.unsplash.com/photo-1623788452350-4c8596ff40bb?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" class="d-block w-100" alt="..." />
                            <div class="carousel-caption d-none d-md-block">
                                <h5 className='moving-text-bold'>Experience Unforgettable Events</h5>
                                <p className='moving-text'>"Your One-Stop Destination for Seamless Event Planning and Execution</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="https://images.squarespace-cdn.com/content/v1/570d4b9e59827eec1255c050/1560180276980-7G0X2O051WTLMQJRCWHO/INDIAN+WEDDING+RECEPTION+PARTY.JPG" class="d-block w-100" alt="..." />
                            <div class="carousel-caption d-none d-md-block">
                                <h5 className='moving-text-bold'>Explore a World of Events</h5>
                                <p className='moving-text'>From Corporate Conferences to Celebratory Parties</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="https://static.toiimg.com/photo/msid-61830567,width-96,height-65.cms" class="d-block w-100" alt="..." />
                            <div class="carousel-caption d-none d-md-block">
                                <h5 className='moving-text-bold' >Transform Your Vision into Reality</h5>
                                <p className='moving-text'>Effortless Planning, Impeccable Execution – Let Your Events Shine!</p>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>

            <div style={{ marginTop: "3rem" }}>
                <div> <b><span>REAL WEDDINGS</span></b></div>
                <div style={{ marginTop: "2rem" }}>
                    <img src='https://www.brides.com/thmb/EL2Na5GHsugM9RF4jDIbxxynRkQ=/3000x0/filters:no_upscale():max_bytes(200000):strip_icc():format(webp)/mira-rohan_01-53f7b9711a2544f7a67432ea57981807.jpg' />

                    <h2 style={{ marginTop: "2rem" }}>A Lakeside Wedding Weekend With South Asian Traditions</h2>
                    <p style={{ margin: "40px", fontFamily: "Myriad Pro Semibold Italic", textAlign: "justify", textAlignLast: "center" }}>We're thrilled to be a part of your journey as you plan the wedding of your dreams. Our wedding planning platform is designed to make the process as seamless and enjoyable as possible.Here's what you can expect:

                        Personalized Dashboard:

                        Create your account and enjoy a personalized dashboard where you can track your wedding planning progress, manage tasks, and access important information at a glance.
                        Budgeting Made Easy:

                        Use our budgeting tool to estimate and manage your wedding expenses. Input planned and actual costs, and let our visual representation guide you in staying on budget.
                        Comprehensive Checklist:

                        Our comprehensive wedding checklist covers everything from the engagement to the honeymoon. Mark completed tasks and receive timely reminders for upcoming ones.
                        Vendor Directory:

                        Explore our curated vendor directory featuring reviews and ratings for various services. Connect with vendors directly through our platform to simplify the booking process.
                        Venue Search and Booking:

                        Find your dream venue with ease using our search and filter options. Once you've made your choice, secure your booking directly through our platform.
                        Guest List Manager:

                        Easily manage your guest list, track RSVPs, and organize seating arrangements all in one place.
                        Timeline and Calendar:

                        Stay organized with our timeline and calendar features, outlining key milestones and deadlines throughout your wedding planning journey.
                        Digital Invitations and RSVP:

                        Create and send beautiful digital invitations, and track RSVPs effortlessly.
                        Registry Integration:

                        Link or create wedding registries from your favorite stores, all in one convenient location.
                        Photo and Video Gallery:
                        - Cherish and share your journey with a dedicated space for photos and videos, from engagement highlights to the big day itself.

                        Live Streaming:
                        - For guests who can't attend in person, consider our live streaming feature to share your special moments virtually.

                        Weather Updates:
                        - Stay informed about the weather on your wedding day with real-time updates.

                        Travel and Accommodation Information:
                        - Help your out-of-town guests with travel and accommodation information, suggesting nearby hotels and transportation options.</p>
                </div>
            </div>

            <div style={{ marginTop: "3rem", display: "", marginLeft: "2rem" }}>
                <b> <span>PARTY CELEBRATION</span></b>
                <div style={{ display: "grid", alignItems: "center", gridTemplateColumns: "1fr 1fr 1fr", columnGap: "100px" }}>

                    <div style={{ marginTop: "2rem", marginLeft: "7rem" }}>
                        <img src='https://weddingsagun.com/wp-content/uploads/2022/07/Birthday-party-I3.jpg' style={{ borderRadius: "20px" }} />
                    </div>

                    <div class="card" style={{ width: "25rem", borderRadius: "20px", border: "solid black", fontFamily: "Myriad Pro Semibold Italic", fontSize: "18px" }}>
                        <div class="card-body">
                            <p class="card-text"> <span>"Transforming birthdays into unforgettable moments! 🎉 From theme selection to engaging activities, our guide ensures your celebration is a hit. Let's plan, decorate, and create cherished memories together. Make your birthday extraordinary with our expert tips! 🎂 #BirthdayCelebration #EventPlanning"</span></p>

                        </div>
                    </div>
                </div>
            </div>
            {/* style={{ display: "grid", alignItems: "center", gridTemplateColumns: "1fr 1fr 1fr", columnGap: "1px", marginTop:"2rem" }} */}
            <h3 style={{ marginTop: "2rem" }}> Gallery</h3>
            <div style={{ display: "flex", justifyContent: "space-around", flexDirection: "row", marginTop: "2rem", marginBottom: "2rem" }} >

                <img src='https://shaadiwish.com/blog/wp-content/uploads/2022/01/Couple-Candid-Photography.jpg' style={{ height: "400px", width: "350px" }} />
                <img src='https://st2.depositphotos.com/1039098/6918/i/950/depositphotos_69188009-stock-photo-indian-wedding-ceremony.jpg' style={{ height: "400px", width: "350px" }} />
                <img src='https://images.unsplash.com/photo-1555244162-803834f70033?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8Y2F0ZXJpbmd8ZW58MHx8MHx8fDA%3D' style={{ height: "400px", width: "350px" }} />
                <img src='https://static8.depositphotos.com/1594308/1073/i/380/depositphotos_10734011-stock-photo-cocktail-party.jpg' style={{ height: "400px", width: "350px" }} />        </div>

            {/* <div> */}
            {/* <img src='https://img.freepik.com/premium-photo/wedding-food-catering-services-reception-table-wedding-ceremony-party_37778-949.jpg' />
                <img src='https://images.unsplash.com/photo-1555244162-803834f70033?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8Y2F0ZXJpbmd8ZW58MHx8MHx8fDA%3D' /> */}
            {/* <span>DELICIOUS FOOD SERVICES</span> */}
            {/* <p>"Elevate your events with our culinary expertise! 🍽️ From exquisite menus to seamless service, we turn every occasion into a gastronomic delight. Experience the perfect blend of flavors and professionalism with our food services. Your taste buds deserve the best! #CateringExcellence #EventDining"</p> */}
            {/* </div>style={{ display: "flex", backgroundColor: "#2F333B", color: "white", fontFamily: "Myriad Pro Semibold Italic" }} */}
            {/* <div className='footer'  style={{display:"flex" , flexDirection :"row", justifyContent : "space-evenly", backgroundColor: "#2F333B", }}>
                <div style={{ display: "flex", flexDirection: "column", color: 'white', marginTop : "2rem" }}>
                    <h4>About Us</h4>
                    <h4>Payments</h4>
                    <h4>Help</h4>
                    <h4>
                        Social
                        
                    </h4>
                </div>
                <div style={{marginTop : "2rem"}} ><span style={{ color: "white" }}><b>Office Address:</b></span><br />
                    <p style={{color : "white"}}>OUR'S Event Management,<br />Survey No. 126P, <br />PSR Prime Tower, <br />beside DLF CYBER CITY, <br />Gachibowli, Hyderabad, Telangana,500032 </p>
                </div>

            </div> */}
            <div className="container4" >
    <div className="about" ><b>ABOUT</b><br/>
      <a href="#">Contact us</a><br/>
      <a href="/About">About us</a><br/>
      <a href="#">Careers</a><br/>
      <a href="#">Stories</a><br/>
      <a href="#">Press</a><br/>
    </div>
    <div className="about" ><b>HELP</b><br/>
      <a href="#">Payments</a><br/>
      <a href="#">Confirmation</a><br/>
      <a href="#">Cancellation</a><br/>
      <a href="#">Amount Returns</a><br/>
    
    </div>
    <div className="about" ><b>CONSUMER POLICY</b><br/>
      <a href="#">Cancellation</a><br/>
      <a href="#">Terms of Use</a><br/>
      <a href="#">Security</a><br/>
      <a href="#">Privacy</a><br/>
      
    </div>
    <div className="about"><b>SOCIAL</b><br/>
      <a href="#"> Facebook</a><br/>
      <a href="#"> Twitter</a><br/>
      <a href="#">  Instagram</a><br/>
      <img className='pic' src='https://t4.ftcdn.net/jpg/03/92/71/99/360_F_392719944_L0LYv3e7QozB2tsj3CfUN0HPC8eZQOWb.jpg'/>
    </div><br/>
    
    <div className="mail" ><b>Contact us:</b><br/>
    <a href="#">eventmanagement@gmail.com</a><br/>
    <a href="#">7675948188</a><br/>
      <p>Event Management,
      Hyderabad,500032 ,<br/>
      Telangana, India </p><br/>
    </div>
    
  </div>

        </div>
    )
}
