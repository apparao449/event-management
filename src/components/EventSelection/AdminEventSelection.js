import React, { useState } from 'react';
import './EventSelection.css';
import './AdminEventSelection.css';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepButton from '@mui/material/StepButton';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import BirthdayThemeSelection from '../BirthdayThemeSelection/BirthdayThemeSelection';
import { HousewarmingTheme } from '../HousewarmingTheme/HousewarmingTheme';
import WeddingThemeSelection from '../WeddingThemeSelection/WeddingThemeSelection';
import Photography from '../Photography/Photography'
import FoodSelection from '../FoodSelection/FoodSelection';



const steps = ['Event Selection', 'Theme Selection', 'Photography', 'Food', 'Payment'];


const AdminEventSelection = () => {
    const [activeStep, setActiveStep] = React.useState(0);
    const [completed, setCompleted] = React.useState({});
    const [chipArray, setChipArray] = useState([
        { name: 'Wedding', selected: true },
        { name: 'Birthday', selected: false },
        { name: 'House warming', selected: false }

    ])
    const [ selectedEvent, setSelectedEvent] = useState ("Wedding");
    const [event, setEvent] = useState({
        eventType : "wedding",
        
    })
    const totalSteps = () => {
        return steps.length;
    };

    const completedSteps = () => {
        return Object.keys(completed).length;
    };

    const isLastStep = () => {
        return activeStep === totalSteps() - 1;
    };

    const allStepsCompleted = () => {
        return completedSteps() === totalSteps();
    };

    const handleNext = () => {
        const newActiveStep =
            isLastStep() && !allStepsCompleted()
                ? // It's the last step, but not all steps have been completed,
                // find the first step that has been completed
                steps.findIndex((step, i) => !(i in completed))
                : activeStep + 1;
        setActiveStep(newActiveStep);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleStep = (step) => () => {
        setActiveStep(step);
    };

    const handleComplete = () => {
        const newCompleted = completed;
        newCompleted[activeStep] = true;
        setCompleted(newCompleted);
        handleNext();
    };
    const handleSubmit = () =>{
        
    }

    const handleReset = () => {
        setActiveStep(0);
        setCompleted({});
    };
    return (
        <div id='aes'>
        <nav id='nav1' class="navbar navbar-expand-lg ">
                <a class="navbar-brand" href="#"><em>Event Management</em></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
                    <ul class="navbar-nav mr-auto">
                        <li style={{marginLeft:"300px"}} class="nav-item">
                            <a class="nav-link" href="/fetch">User Data</a>
                        </li>
                        <li style={{marginLeft:"330px"}} class="nav-item">
                            <a class="nav-link" href="/eventfetch">Events Registered</a>
                        </li>
                        <li style={{marginLeft:"330px"}} class="nav-item">
                            <a class="nav-link" href="/">logout</a>
                        </li>
                    </ul>
                    {/* <input style={{ marginLeft: "550px" }} class="form-control mr-sm-2" type="search" placeholder="Search" /> */}
                    {/* <button style={{ marginLeft: "20px", marginRight: "10px" }} class="btn btn-success" type="submit">Search</button> */}
                </div>
            </nav>
        <Box sx={{ width: '100%' }} style={{ padding: "8rem" }}>
            <Stepper nonLinear activeStep={activeStep}>
                {steps.map((label, index) => (
                    <Step key={label} completed={completed[index]}>
                        <StepButton color="inherit" onClick={handleStep(index)}>
                            {label}
                        </StepButton>
                    </Step>
                ))}
            </Stepper>
            <div>
                {allStepsCompleted() ? (
                    <React.Fragment>
                        <Typography sx={{ mt: 2, mb: 1 }}>
                            All steps completed - you&apos;re finished
                        </Typography>
                        <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
                            <Box sx={{ flex: '1 1 auto' }} />
                            <Button onClick={handleReset}>Reset</Button>
                        </Box>
                    </React.Fragment>
                ) : (
                    <React.Fragment>
                        <Typography sx={{ mt: 2, mb: 1, py: 1 }}>
                            {/* Step {activeStep + 1} */}
                            {activeStep + 1 === 1 ?

                                <div >
                                    

                                    <Stack direction="row" spacing={1}  style={{display:"flex", justifyContent: "space-around", paddingTop: "2rem"}}>
                                        {chipArray.map((item, index) => (
                                            <Chip label={item.name} variant="outlined" onClick={() => {
                                                chipArray.forEach((c, idx) => {
                                                    if (idx === index) {
                                                        chipArray[index].selected = !chipArray[index].selected;
                                                    }
                                                    else {
                                                        c.selected = false
                                                    }

                                                })
                                                setSelectedEvent(chipArray[index].name)
                                                // {console.log(selectedEvent)}
                                                setChipArray([...chipArray])
                                            }}
                                                size='large' className='template_chip'
                                                style={{
                                                    backgroundColor: item.selected ? '#1976d2' : '#fff',
                                                    color: item.selected ? "#fff" : "#1976d2"
                                                }}
                                            />))}
                                    </Stack>
                                    <div className='loc-input'  style={{paddingTop :"5rem"}}>
                                        <TextField id="outlined-basic" label="Enter the location for your big day" variant="outlined"
                                            style={{
                                                width: "40rem",
                                            }} />
                                    </div>

                                </div>

                                : ""}
                              {activeStep+1 === 2 ? 
                                selectedEvent === "Birthday" ? <BirthdayThemeSelection/> :
                                selectedEvent === "House warming" ? <HousewarmingTheme />
                                 : selectedEvent === "Wedding" ? <WeddingThemeSelection /> :""
                               : ""}
                               { activeStep + 1 === 3 ? <Photography /> : "" }
                               { activeStep + 1 === 4 ? <FoodSelection/> : "" }
                               { activeStep + 1 === 5 ? <Photography /> : "" }
                               
                        </Typography>
                        <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
                            <Button
                                color="inherit"
                                disabled={activeStep === 0}
                                onClick={handleBack}
                                sx={{ mr: 1 }}
                            >
                                Back
                            </Button>
                            <Box sx={{ flex: '1 1 auto' }} />
                            <Button onClick={handleNext} sx={{ mr: 1 }}>
                                Next
                            </Button>
                            {activeStep !== steps.length &&
                                (completed[activeStep] ? (
                                    <Typography variant="caption" sx={{ display: 'inline-block' }}>
                                        Step {activeStep + 1} already completed
                                    </Typography>
                                ) : (
                                    <div>

                                    <Button onClick={handleComplete}>
                                        {completedSteps() === totalSteps() - 1
                                            ? 'Finish'
                                            : 'Complete Step'}
                                    </Button>
                                    <Button onClick={handleComplete}>
                                        Submit
                                    </Button>
                                            </div>
                                    
                                ))}
                        </Box>
                    </React.Fragment>
                )}
            </div>
        </Box>
        <br/>
        <br/>
        </div>
    );
}

export default AdminEventSelection;