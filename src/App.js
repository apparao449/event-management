import logo from './logo.svg';
import './App.css';
import  Navbar from './components/Navbar/Navbar';
import EventSelection from './components/EventSelection/EventSelection';
import BirthdayThemeSelection from './components/BirthdayThemeSelection/BirthdayThemeSelection';
import { HousewarmingTheme } from './components/HousewarmingTheme/HousewarmingTheme';
import WeddingThemeSelection from './components/WeddingThemeSelection/WeddingThemeSelection';
import FoodSelection from './components/FoodSelection/FoodSelection';
import {BrowserRouter,Routes,Route} from 'react-router-dom'
import Registration from './components/Registration/Registration';
import { HomePage } from './components/HomePage/HomePage';
import Login from './components/Login/Login';
import Login2 from './components/Login/Login2';
import AdminEventSelection from './components/EventSelection/AdminEventSelection';
import Fetch from './Fetch/Fetch';
import Demo from './components/Demo/Demo';
import EventFetch from './Fetch/Eventsfetch';


function App() {
  return (
    <div className="App">
      {/* <Demo/> */}

      <BrowserRouter>
    <Routes>
    <Route path='/' element={<HomePage/>}></Route>
      <Route path='/register' element={<Registration/>}></Route>
      <Route path='/adminlogin' element={<Login/>}></Route>
      <Route path='/userlogin' element={<Login2/>}></Route>
      <Route path='/eventselection' element={<EventSelection/>}></Route>
      <Route path='/admineventselection' element={<AdminEventSelection/>}></Route>
      <Route path='/fetch' element={<Fetch/>}></Route>
      <Route path='/eventfetch' element={<EventFetch/>}></Route>
    </Routes>
     </BrowserRouter> 
      {/* <BirthdayThemeSelection/> */}
      {/* <HousewarmingTheme/> */}
      {/* <WeddingThemeSelection /> */}
      {/* /* <FoodSelection/> */}

    </div>
  );
}

export default App;
